require 'rails_helper'

RSpec.describe GithubController, type: :controller do
	describe "GithubController" do
		describe "Routes" do
			it "route get users." do
				expect(:get => "/users").to route_to(:controller => "github",
													:action => "users",
													:format => "json"
													)
			end

			it "route post register." do
				expect(:post => "/register").to route_to(:controller => "github",
														:action => "register",
														:format => "json"
														)
			end

			it "route delete clean." do
				expect(:delete => "/clean").to route_to(:controller => "github",
														:action => "clean",
														:format => "json"
														)
			end
		end

		it "doesn't load all users or fail to fill userAndRepos #users." do
			user1 = User.new(:name => "user1", 
							 :stars => 0, 
							 :id => 1)
			user1.save
			repository = Repository.new(:name => "repository", 
										:date => Time.now, 
										:user_id => 1)
			repository.save
			user2 = User.new(:name => "user2", 
							 :stars => 5, 
							 :id => 2)
			user2.save
			user3 = User.new(:name => "user3", 
							 :stars => -2, 
							 :id => 3)
			user3.save
			get :users, format: :json
			users = JSON.parse(response.body)
			expect(users.length).to eq 2
			expect(users[0]["name"]).to eq "user1"
			expect(users[0]["repos"]).to eq 1
			expect(users[0]["stars"]).to eq 0
			expect(users[1]["name"]).to eq "user2"
			expect(users[1]["repos"]).to eq 0
			expect(users[1]["stars"]).to eq 5
		end

		it "register user or repository fail #register." do
			json = createJson
			post :register, json, format: :json
			expect(User.all.length).to eq 1
			expect(User.all[0].name).to eq "user1"
			expect(User.all[0].stars).to eq 9
			expect(Repository.all.length).to eq 1
			expect(Repository.all[0].name).to eq "repository1"
			expect(Repository.all[0].date).to eq "2016-08-28 00:23:12"
			expect(Repository.all[0].user.id).to eq User.all[0].id
		end

		it "can't register two times same user #register." do
			user1 = User.new(:name => "user1", 
							 :stars => 5)
			user1.save
			user2 = User.new(:name => "user2", 
							 :stars => 7)
			user2.save
			json = createJson
			post :register, json, format: :json
			expect(User.all.length).to eq 2
			expect(User.where(:name => "user1").first.stars).to eq 9
		end

		it "Clean database fail #clean." do
			json = createJson
			delete :clean, json, format: :json
			expect(User.all.length).to eq 0
			expect(Repository.all.length).to eq 0
		end

		def createJson
			{:repos => [{:name => "repository1", 
						 :stargazers_count => 9, 
						 :created_at => "2016-08-28 00:23:12", 
						 :owner => {:login => "user1"}}]}
		end
	end
end

