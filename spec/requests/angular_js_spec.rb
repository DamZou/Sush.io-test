require 'rails_helper'

feature "Angular", js: true do
  	scenario "Can't open home template." do
  		visit "/#"
  		expect(page).to have_content("Nickname")
  		expect(page).to have_content("Get user repos")
  	end

  	scenario "Can't open users template." do
  		visit "/#users"
  		expect(page).to have_content("No user register in database.")
  	end

  	scenario "Can't check navBar in home template." do
  		visit "/#"
  		expect(page).to have_content("Sush.io-Test")
  		expect(page).to have_content("Home")
  		expect(page).to have_content("Users")
  	end

  	scenario "Can't check navBar in users template." do
  		visit "/#users"
  		expect(page).to have_content("Sush.io-Test")
  		expect(page).to have_content("Home")
  		expect(page).to have_content("Users")
  	end

  	scenario "Navbar links fail" do
  		visit "/#users"
  		click_on "Home"
  		expect(page).to have_content("Nickname")
  		expect(page).to have_content("Get user repos")
  		click_on "Users"
  		expect(page).to have_content("No user register in database.")
  	end

  	scenario "Get user repos fail." do
  		visit "/#"
  		fill_in "nickname", with: "Damzou"
  		click_on "Get user repos"
  		expect(page).to have_content("lol")
  		expect(page).to have_content("salut")
  		expect(page).to have_content("yuopi")
  	end

  	scenario "Register user or repos fail." do
  		visit "/#"
  		fill_in "nickname", with: "Damzou"
  		click_on "Get user repos"
  		click_on "Register user and repos"
  		expect(page).to have_content("User has been saved")
  		expect(page).to have_content("3 repositories have been saved.")
  		visit "/#users"
  		expect(page).to have_content("damzou")
  	end

  	scenario "Can register user two times." do
  		visit "/#"
  		fill_in "nickname", with: "Damzou"
  		click_on "Get user repos"
 		click_on "Register user and repos"
 		expect(page).to have_content("User has already registered.")
 	end

 	scenario "CleanDB fail." do
 		visit "/#users"
 		click_on "Clean database"
 		expect(page).to have_content("No user register in database.")
 	end
end
