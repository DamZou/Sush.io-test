require 'rails_helper'

RSpec.describe Repository, type: :model do
  	it "Date cannot be blank." do
  		createUser
  		repository = createRepository
  		repository[:date] = nil
		expect(Repository.new(repository)).to_not be_valid
	end

	it "Name cannot be blank." do
		createUser
		repository = createRepository
		repository[:name] = nil
		expect(Repository.new(repository)).to_not be_valid
	end

	it "User_id cannot be blank." do
		createUser
		repository = createRepository
		repository[:user_id] = nil
		expect(Repository.new(repository)).to_not be_valid
	end

	it "User_id must be integer." do
		createUser
		repository = createRepository
		repository[:user_id] = "user_id"
		expect(Repository.new(repository)).to_not be_valid
	end

	it "User_id must be greater or equal to 0" do
		createUser
		repository = createRepository
		repository[:user_id] = -5
		expect(Repository.new(repository)).to_not be_valid
	end

	it "Date must be date" do
		createUser
		repository = createRepository
		repository[:date] = "Time.now"
		expect(Repository.new(repository)).to_not be_valid
	end

	it "User must exist" do
		repository = createRepository
		expect(Repository.new(repository)).to_not be_valid
	end

	def createRepository
		{:name => "repository", 
		 :date => Time.now, 
		 :user_id => 1}
	end

	def createUser
		user = User.new(:name => "user", :stars => 5, :id => 1)
		user.save
	end
end
