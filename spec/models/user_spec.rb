require 'rails_helper'

RSpec.describe User, type: :model do
	it "Stars cannot be blank." do
		user = createUser
		user[:stars] = nil
		expect(User.new(user)).to_not be_valid
	end

	it "Name cannot be blank." do
		user = createUser
		user[:name] = nil
		expect(User.new(user)).to_not be_valid
	end

	it "Stars must be integer." do
		user = createUser
		user[:stars] = "stars"
		expect(User.new(user)).to_not be_valid
	end

	it "Stars must be greater or equal to 0." do
		user = createUser
		user[:stars] = -5
		expect(User.new(user)).to_not be_valid
	end

	def createUser
		{:name => "user", 
		 :stars => 2}
	end

end
