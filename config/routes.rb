Rails.application.routes.draw do

# Routes controllers
	post '/register', to: 'github#register', defaults: { :format => 'json' }
	get '/users', to: 'github#users', defaults: { :format => 'json' }
	delete '/clean', to: 'github#clean', defaults: { :format => 'json' }
	
# Route to get angular templates
	root to: 'application#angular'
	  
end
