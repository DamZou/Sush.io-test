class User < ApplicationRecord

	has_many :repositories, dependent: :destroy

	validates :name, presence: true, uniqueness: true
	validates :stars, presence: true, numericality: {greater_than_or_equal_to: 0}

end
