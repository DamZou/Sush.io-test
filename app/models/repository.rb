class Repository < ApplicationRecord

	belongs_to :user

	validates :name, presence: true
	validates :date, presence: true, timeliness: {type: :datetime}
	validates :user_id, presence: true, numericality: {greater_than_or_equal_to: 0}

end
