sushTest.controller('usersCtrl',function($scope, $http, $state){
// Get all users register in database.
	$http.get('/users.json')
	.then(function(usersAndRepos){
		$scope.usersAndRepos = usersAndRepos.data;
	});
// clean database.
	$scope.CleanDB = function(){
		$http.delete('/clean.json')
		.then(function(){
			$state.reload();
		});
	};	
});