sushTest.controller('homeCtrl',function($scope, $http){
// Get all repos for a user.
	$scope.GetUserRepos = function(){
		$http.get('https://api.github.com/users/' + $scope.nickname + '/repos')
		.then(function(repos){
			$scope.repos = repos.data;
			$scope.error = null;
			$scope.msg = null;
			$scope.repositories = null;
			})
		.catch(function(){
			$scope.error = "User does not have repos or user does not exist.";
			$scope.repos = null;
			$scope.msg = null;
			$scope.repositories = null;
		});
	};
// Register a user and their repos.
	$scope.Register = function(){
		var repos = {repos: $scope.repos};
		$http.post('/register.json', repos)
		.then(function(resp){
			$scope.msg = resp.data.msg;
			$scope.repositories = resp.data.repositories;
			$scope.repos = null;
			$scope.error = null;
		});
	};	
});