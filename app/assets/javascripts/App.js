// config angular application
var sushTest = angular.module('sushTest',['ui.router', 'templates']);

sushTest.config(['$stateProvider','$urlRouterProvider',
 	  function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('users', {
            url: '/users',
            templateUrl: 'angular/users/_users.html'
        })
        .state('home', {
            url: '/',
            templateUrl: 'angular/home/_home.html'
        })
    $urlRouterProvider.otherwise('/');
 }])