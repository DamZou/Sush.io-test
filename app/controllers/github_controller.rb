class GithubController < ApplicationController

# Register user in database if doesn't exist or update stars number if already exist.
# Register repository in database. Many repositories can have same name but different user_id. 
	def register	
		if params["repos"] && params["repos"][0]["owner"]["login"] != nil # Prevent crash if params["repos"] and/or params["repos"][0]["owner"]["login"] are nil
			user = User.new(:name => params["repos"][0]["owner"]["login"].downcase, 
							:stars => starsCounter(params["repos"]))
			msg = "User has been saved."
			if !user.save
				user = User.where(:name => params["repos"][0]["owner"]["login"].downcase).first
				user.update(:stars => starsCounter(params["repos"]))
				msg = "User has already registered."
			end
			repositories = 0
			params["repos"].each do |repo|
				repository = Repository.new(:name => repo["name"], 
											:date => repo["created_at"], 
											:user_id => user.id)
				if Repository.where(:name => repository.name, 
									:user_id => repository.user_id) == []
					repository.save
					repositories += 1
				end
			end
		end
		render :json => {:msg => msg, :repositories => repositories}
	end

# Get all users register in database.
# Each in controller because foreign key fail in angular front.
	def users
		users = User.all
		usersAndRepos = []
		if users.length != 0
			users.each do |user|
				userAndRepos = {}
				userAndRepos["name"] = user.name	
				if user.repositories != nil # Prevent crash if user.repositories is nil
					userAndRepos["repos"] = user.repositories.length
				end
				userAndRepos["stars"] = user.stars
				usersAndRepos.push(userAndRepos)
			end 
		end
		render :json => usersAndRepos
	end

# Clean database.
	def clean
		User.destroy_all
		render :json => ""
	end

	private

# Count stars for users.
	def starsCounter(repos)
		stars = 0
		repos.each do |repo|
			if repo["stargazers_count"] != nil # Prevent crash if repo["stargazers_count"] is nil
				stars += repo["stargazers_count"].to_i
			end
		end
		stars
	end

end
