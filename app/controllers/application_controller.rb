class ApplicationController < ActionController::Base
  	
  	protect_from_forgery unless: -> { request.format.json? }

	def angular
	  	render 'layouts/application'
	end
	
end
